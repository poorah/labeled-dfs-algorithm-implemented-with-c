#include<iostream>
#include<map>
#include<list>
#include<iterator>

using namespace std ;

class Graph {
	public :
		//Properties :
		map<int, list<int>> adj;
		map<int, bool> visited;
		map<int, int> label;
		int cc;
		//Methods:
		void addEdge(int v, int w);
		void Explore(int v);
		void DFS();
		void newExplore(int v);
		void newDFS();
};

void Graph::addEdge(int v, int w) {
	//TODO
	adj[v].push_back(w);
	adj[w].push_back(v);
}


void Graph::newExplore(int v) {
	//TODO
	visited[v] = true;
	label[v] = cc ;
	cout << "( " << v << " , " << cc << " )" << " -> ";
	list<int>::iterator i;
	
	for(i = adj[v].begin(); i != adj[v].end(); i++) {
		if(!visited[*i]) {
			newExplore(*i) ;
		}
	}
}

void Graph::newDFS() {
	//TODO
	cc = 1;
	map<int, list<int>>::iterator i;
	
	for(i = adj.begin(); i != adj.end(); i++) {
		visited[i->first] = false ;
	}
	
	for(i = adj.begin(); i != adj.end(); i++) {
		if(!visited[i->first]) {
			newExplore(i->first);
			cc += 1;
			cout << "end" << endl;
		}
	}
}

int main() {

    Graph g ;
    
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(3, 1);
    g.addEdge(4, 5);
    g.addEdge(5, 6);
    g.addEdge(6, 4);
    g.addEdge(7, 8);
    g.addEdge(10, 10);

    g.newDFS() ;
    //cout << "end" ;
    
    return 0;
}
